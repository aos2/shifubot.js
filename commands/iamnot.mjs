import { config } from "../config.mjs";
import * as console from "../log.mjs";

export default {
  name: "iamnot",
  description: "Removes the specified role from the user.",
  whitelist_channels: [config.roles_channel],
  async run(message, args) {
    const managedRoles = config.roles_regions.concat(config.all_chars);
    const undesiredRole = args.join(" ");
    if (managedRoles.includes(undesiredRole)) {
      await message.member
        .removeRole(
          message.guild.roles.find(role => role.name === undesiredRole)
        )
        .catch(console.error);
    }
    await message.delete(60000).catch(console.error);
  }
};
