import { config } from "../config.mjs";
import { intersection } from "../utils.mjs";
import * as console from "../log.mjs";

export default {
  name: "iam",
  description: "Assign user their requested role.",
  whitelist_channels: [config.roles_channel],
  async run(message, args) {
    const roles = message.guild.roles;
    const desiredRole = args.join(" ");
    const user = message.member;
    // If user wants to assign region, remove current (if any) then add role.
    if (config.roles_regions.includes(desiredRole)) {
      const filtered = intersection(user.roles, config.roles_regions, "name");
      if (filtered.size)
        await user
          .removeRole(roles.find(role => role.name === filtered.first().name))
          .catch(console.error);
      await user
        .addRole(roles.find(role => role.name === desiredRole))
        .catch(console.error);
    }

    // If user wants to assign character, check if already assigned MAX such roles.
    else if (config.all_chars.includes(desiredRole)) {
      const filtered = intersection(user.roles, config.all_chars, "name");
      if (filtered.size >= config.roles_chars_max) {
        await message
          .reply(
            `there's a maximum of ${config.roles_chars_max} character roles.
                    Use \`${config.prefix}iamnot\` to remove roles.`
          )
          .then(sent => sent.delete(60000))
          .catch(console.error);
      } else {
        await user
          .addRole(roles.find(role => role.name === desiredRole))
          .catch(console.error);
      }
    }
    await message.delete(60000).catch(console.error);
  }
};
