FROM mhart/alpine-node:latest
WORKDIR /app
RUN apk add --no-cache paxctl && \
    paxctl -cm `which node`
COPY package.json .
RUN npm install --only=prod
COPY . .
CMD ["sh", "-c", "syslogd && npm start"]
