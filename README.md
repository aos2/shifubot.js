## shifubot.js

[![pipeline status](https://gitlab.com/aos2/shifubot.js/badges/master/pipeline.svg)](https://gitlab.com/aos2/shifubot.js/commits/master)

A bot meant to organize and facilitate activities in the [Acceleration of Suguri 2 community Discord server][server]. Others can use this as a template for their own bots — you can pick and choose the functionality you want to keep. Furthermore, we supply a Dockerfile to run the bot in a [minimal Alpine Linux environment][images]. This lets us deploy Shifu Bot to a [Kubernetes][kubernetes] cluster and enjoy auto-repairing nodes, much like its [namesake][shifu-robot].

If you want to run your own bot, remember to supply your own [secret token][secret] in `index.mjs` and adjust `config.mjs`.

## requirements

* [docker]
* [node] for development

The bot itself requires

* [discord.js]

## usage

```sh
git clone git@gitlab.com:aos2/shifubot.js.git
cd shifubot.js
docker build -t shifu-bot .
docker run shifu-bot
```

## development

Install development tools with `npm install --only=dev` which includes a pre-commit hook that checks for errors reported by [prettier] and [eslint]. Use `npm run lint` to automatically adjust files in-place.

Another thing to note is that we're a little bit ahead of the curve adopting [ECMAScript Modules][esmodules] (`import`, `export`) over the more prevalent CommonJS modules (`require`, `module.exports`). ES Modules are on track to become the de facto standard.

You're welcome to discuss Shifu Bot in the `#project_discussion` channel on [Discord][server].

[discord.js]: https://github.com/discordjs/discord.js/
[docker]: https://www.docker.com/
[eslint]: https://eslint.org/
[esmodules]: https://nodejs.org/dist/latest-v10.x/docs/api/esm.html
[images]: https://hub.docker.com/r/mhart/alpine-node/
[kubernetes]: https://kubernetes.io/
[node]: https://nodejs.org/en/
[prettier]: http://prettier.io/
[secret]: https://discordapp.com/developers/applications/
[server]: https://aos2.gitlab.io/#community
[shifu-robot]: https://onehundredpercentorangejuice.fandom.com/wiki/Shifu_Robot
