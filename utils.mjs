/*
 * Returns a Collection(Array) containing elements identified
 * in the Collection(Array) col that are also present in an Array arr.
 * col: Collection<Key, Value> | Array
 * arr: Array
 * prop?: String, the property name of the Value(element) to compare
 */
export function intersection(col, arr, prop) {
  if (prop) return col.filter(x => arr.includes(x[prop]));
  else return col.filter(x => arr.includes(x));
}
