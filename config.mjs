export const config = {};

// Shifu-Bot
config.prefix = "!";

// Game
const allChars = [
  "Alte",
  "Hime",
  "Iru",
  "Kae",
  "Kyoko",
  "Mira",
  "Nanako",
  "Nath",
  "Saki",
  "Sham",
  "Sora",
  "Star Breaker",
  "Suguri",
  "Sumika",
  "Tsih"
];
config.all_chars = allChars;
config.all_random = allChars.concat(["Random"]);

// Server
config.roles_channel = "209090810879279114";
config.roles_chars_max = 3;
config.roles_regions = [
  "Europe",
  "NA - East Coast",
  "NA - Mid",
  "NA - West Coast",
  "South America",
  "Asia",
  "Oceania",
  "Africa"
];

// Miscellaneous
config.rps = ["Rock", "Paper", "Scissors"];
