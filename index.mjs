import Discord from "discord.js";
import { readdirSync } from "fs";
import secret from "./secret.json";
import { config } from "./config.mjs";
import * as console from "./log.mjs";

const client = new Discord.Client();
const commands = new Map();

async function setup() {
  const commandFiles = readdirSync(`./commands`).filter(file =>
    file.endsWith(".mjs")
  );
  for (const file of commandFiles) {
    const command = (await import("./commands/" + file)).default;
    commands.set(command.name, command);
  }
}

client.on("ready", () => {
  console.log(
    `Running as ${client.user.tag} serving ${client.guilds.size} communities!`
  );
  client.user.setActivity("Turbo Charged").catch(console.error);
});

client.on("message", async message => {
  if (message.author.bot || !message.content.startsWith(config.prefix)) return;
  const args = message.content
    .slice(config.prefix.length)
    .trim()
    .split(/ +/g);
  let command = args.shift().toLowerCase();

  if (commands.has(command)) {
    command = commands.get(command);
    if (command.whitelist_channels.includes(message.channel.id)) {
      command.run(message, args).catch(console.error);
    }
  }
});

client.on("error", e => console.error(e));
client.on("warn", e => console.warn(e));
client.on("debug", e => console.info(e));

setup();
client.login(secret.token);
