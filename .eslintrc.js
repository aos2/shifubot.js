module.exports = {
  env: {
    es6: true
  },
  extends: ["standard", "prettier"],
  parser: "babel-eslint",
  parserOptions: {
    allowImportExportEverywhere: false
  },
  plugins: ["prettier"],
  rules: {
    "prettier/prettier": "error"
  }
};
